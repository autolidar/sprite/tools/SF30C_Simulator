#include <Arduino.h>

#define ENC_CPR 200   // Encoder counts-per-revolution
#define RGF_HZ 18317  // Rangefinder refresh rate
#define SCAN_HZ 3.   // Number of revolutions per second

// Simulated encoder output pins
#define PIN_ENC_IDX 3
#define PIN_ENC_CHA 4

// Simulated rangefinder output
#define PIN_RGF_SYN 2
#define RGF_SERIAL Serial1



// Encoder counts per second = counts / revolution * revolutions / second
float ENC_HZ = ENC_CPR * SCAN_HZ;
float ENC_US = 1000000. / ENC_HZ;
IntervalTimer encTimer;

// Encoder pulse timing:
//
// state (AB) => idx, cha
// 0 (00) => idx HIGH if ctr == 0 else LOW, cha LOW
// 1 (01) => idx LOW, cha HIGH
// 2 (10) => idx LOW, cha HIGH
// 3 (11) => idx LOW, cha LOW
//
// Therefore:
// idx = (ctr == 0) & (state == 0)
// cha = A ^ B
//
// ctr is incremented on state rollover (every ENC_US microseconds)

struct uint8_b {
  uint8_t b7 : 1;
  uint8_t b6 : 1;
  uint8_t b5 : 1;
  uint8_t b4 : 1;
  uint8_t b3 : 1;
  uint8_t b2 : 1;
  uint8_t b1 : 1;
  uint8_t b0 : 1;
};

union state_t {
  uint8_t byte;
  uint8_b bit;
};

struct enc_data_t {
  state_t state;
  uint8_t ctr;
};

volatile enc_data_t enc_data;

void isr_enc() {
  digitalWriteFast(PIN_ENC_IDX, (enc_data.ctr == 0) & (enc_data.state.byte == 0));
  digitalWriteFast(PIN_ENC_CHA, enc_data.state.byte == 1 || enc_data.state.byte == 2);
  if (++enc_data.state.byte == 4) {
    enc_data.state.byte = 0;
    if (++enc_data.ctr == ENC_CPR) {
      enc_data.ctr = 0;
      digitalWriteFast(LED_BUILTIN, HIGH);
    }
    if (enc_data.ctr == ENC_CPR / 3) {
      digitalWriteFast(LED_BUILTIN, LOW);
    }
  }
}



float RGF_US = 1000000. / RGF_HZ;
IntervalTimer rgfTimer;

volatile bool rgf_transmit;

// Can't do Serial.write() inside an ISR, so
// set a flag to do it in the main loop.
void isr_rgf() {
  digitalWriteFast(PIN_RGF_SYN, LOW);   // SYNC becomes LOW on measurement
  rgf_transmit = true;
}

// Lookup table for fake rangefinder data.
// Simulated reading corresponding to each encoder count (200 total).
float fakeRgfData[] = {
    3,           2.995561965, 2.982287251, 2.960293686, 2.929776486,
    2.891006524, 2.844327926, 2.790155012, 2.728968627, 2.661311865,
    2.587785252, 2.509041416, 2.425779292, 2.33873792,  2.248689887,
    2.156434465, 2.06279052,  1.968589241, 1.874666766, 1.781856759,
    1.690983006, 1.602852109, 1.518246326, 1.437916622, 1.36257601,
    1.292893219, 1.229486757, 1.172919426, 1.12369332,  1.082245374,
    1.048943484, 1.024083238, 1.007885299, 1.00049344,  1.001973272,
    1.012311659, 1.031416839, 1.059119231, 1.095172948, 1.139257973,
    1.190983006, 1.24988893,  1.315452894, 1.387092946, 1.464173205,
    1.5460095,   1.631875447, 1.721008894, 1.812618685, 1.905891687,
    2,           2.094108313, 2.187381315, 2.278991106, 2.368124553,
    2.4539905,   2.535826795, 2.612907054, 2.684547106, 2.75011107,
    2.809016994, 2.860742027, 2.904827052, 2.940880769, 2.968583161,
    2.987688341, 2.998026728, 2.99950656,  2.992114701, 2.975916762,
    2.951056516, 2.917754626, 2.87630668,  2.827080574, 2.770513243,
    2.707106781, 2.63742399,  2.562083378, 2.481753674, 2.397147891,
    2.309016994, 2.218143241, 2.125333234, 2.031410759, 1.93720948,
    1.843565535, 1.751310113, 1.66126208,  1.574220708, 1.490958584,
    1.412214748, 1.338688135, 1.271031373, 1.209844988, 1.155672074,
    1.108993476, 1.070223514, 1.039706314, 1.017712749, 1.004438035,
    1,           1.004438035, 1.017712749, 1.039706314, 1.070223514,
    1.108993476, 1.155672074, 1.209844988, 1.271031373, 1.338688135,
    1.412214748, 1.490958584, 1.574220708, 1.66126208,  1.751310113,
    1.843565535, 1.93720948,  2.031410759, 2.125333234, 2.218143241,
    2.309016994, 2.397147891, 2.481753674, 2.562083378, 2.63742399,
    2.707106781, 2.770513243, 2.827080574, 2.87630668,  2.917754626,
    2.951056516, 2.975916762, 2.992114701, 2.99950656,  2.998026728,
    2.987688341, 2.968583161, 2.940880769, 2.904827052, 2.860742027,
    2.809016994, 2.75011107,  2.684547106, 2.612907054, 2.535826795,
    2.4539905,   2.368124553, 2.278991106, 2.187381315, 2.094108313,
    2,           1.905891687, 1.812618685, 1.721008894, 1.631875447,
    1.5460095,   1.464173205, 1.387092946, 1.315452894, 1.24988893,
    1.190983006, 1.139257973, 1.095172948, 1.059119231, 1.031416839,
    1.012311659, 1.001973272, 1.00049344,  1.007885299, 1.024083238,
    1.048943484, 1.082245374, 1.12369332,  1.172919426, 1.229486757,
    1.292893219, 1.36257601,  1.437916622, 1.518246326, 1.602852109,
    1.690983006, 1.781856759, 1.874666766, 1.968589241, 2.06279052,
    2.156434465, 2.248689887, 2.33873792,  2.425779292, 2.509041416,
    2.587785252, 2.661311865, 2.728968627, 2.790155012, 2.844327926,
    2.891006524, 2.929776486, 2.960293686, 2.982287251, 2.995561965,
};



void setup() {
  pinMode(LED_BUILTIN, OUTPUT);
  digitalWrite(LED_BUILTIN, HIGH);

  pinMode(PIN_ENC_IDX, OUTPUT);
  digitalWrite(PIN_ENC_IDX, LOW);
  pinMode(PIN_ENC_CHA, OUTPUT);
  digitalWrite(PIN_ENC_CHA, LOW);

  pinMode(PIN_RGF_SYN, OUTPUT);
  digitalWrite(PIN_RGF_SYN, HIGH);
  RGF_SERIAL.begin(921600);

  // Run encoder simulation function at 4x the necessary refresh rate
  // (quad-pumping?). This allows us to synchronize generation of all the
  // required waveforms, including the IDX pulse.
  encTimer.begin(isr_enc, ENC_US / 4.);

  rgfTimer.begin(isr_rgf, RGF_US);
}



void loop() {
  if (rgf_transmit) {
    rgf_transmit = false;
    float distance = fakeRgfData[enc_data.ctr];
    
    // Decompose distance into the 2-byte format used by the SF30/C
    uint8_t Byte_H = distance;
    uint8_t Byte_L = (distance - Byte_H) * 256;

    // Send the bytes in the correct order (Byte_L,Byte_H) and flush
    digitalWriteFast(PIN_RGF_SYN, HIGH);  // SYNC becomes HIGH just before TX
    RGF_SERIAL.write(Byte_L);
    RGF_SERIAL.write(Byte_H);
    RGF_SERIAL.flush();
  }
}
